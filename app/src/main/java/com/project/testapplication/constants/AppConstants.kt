package com.project.testapplication.constants

import java.util.concurrent.TimeUnit

object AppConstants {
    const val TIME_OUT: Long = 1
    val TIME_UNIT = TimeUnit.MINUTES
    const val DEFAULT_STRING_VALUE = "non"
    const val DEFAULT_ERROR_VALUE = -2
}
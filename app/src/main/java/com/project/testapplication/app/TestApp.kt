package com.project.testapplication.app

import android.app.Application
import com.blankj.utilcode.util.Utils
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TestApp: Application() {
    override fun onCreate() {
        super.onCreate()
        Utils.init(this)
    }
}
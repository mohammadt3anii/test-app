package com.project.testapplication.data.remote

import com.project.testapplication.models.images.ImageItemResponseItem
import retrofit2.Response
import retrofit2.http.GET

interface ApiService{
    @GET("photos")
    suspend fun listOfImages(): Response<List<ImageItemResponseItem>>
}
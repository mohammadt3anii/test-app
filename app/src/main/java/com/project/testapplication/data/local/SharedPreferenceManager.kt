package com.project.testapplication.data.local

import android.content.SharedPreferences
import com.project.testapplication.ext.get
import com.project.testapplication.ext.set


class SharedPreferenceManager(private val sharedPreferences: SharedPreferenceDataSource,private val preferences: SharedPreferences) {
    companion object {
        private const val SOME_KEY_BOOL = "some_key_bool"
        private const val SOME_KEY_STRING = "some_key_string"
    }

    fun booleanExample(value: Boolean) {
        sharedPreferences.put(SOME_KEY_BOOL, value)
    }

    fun getBooleanExample(): Boolean {
        return sharedPreferences.getBoolean(SOME_KEY_BOOL, false)
    }

    var someKeyString: String?
        get() = preferences[SOME_KEY_STRING]
        set(value) {
            preferences[SOME_KEY_STRING] = value
        }

}
package com.project.testapplication.data.local

interface SharedPreferencesDao {
    fun <T> put(key: String, value: T)
    fun getString(key: String): String?
    fun getBoolean(key: String): Boolean?
    fun getBoolean(key: String, defaultValue: Boolean): Boolean?
    fun getInt(key: String): Int?
    fun getInt(key: String, defaultValue: Int): Int?
    fun getLong(key: String): Long
    fun clear()
    fun deleteKey(key: String)
    fun contain(key: String): Boolean
}
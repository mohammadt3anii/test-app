package com.project.testapplication.data.remote

import com.project.testapplication.BuildConfig
import com.project.testapplication.data.local.SharedPreferenceManager
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor(sharedPreferenceManager: SharedPreferenceManager) :
    Interceptor {
    companion object {
        const val AUTH_VALUE = "Client-ID ${BuildConfig.ACCESS_KEY}"
        private const val AUTHORIZATION = "Authorization"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val requestBuilder = original.newBuilder()
        requestBuilder.addHeader(AUTHORIZATION, AUTH_VALUE)
        requestBuilder.addHeader("Accept", "application/json")
        val request: Request = requestBuilder.build()
        return chain.proceed(request)
    }
}
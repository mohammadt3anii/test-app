package com.project.testapplication.data.local

import android.content.SharedPreferences
import com.blankj.utilcode.util.GsonUtils
import com.project.testapplication.constants.AppConstants


class SharedPreferenceDataSource(private val sharedPreferences: SharedPreferences) : SharedPreferencesDao {

    override fun <T> put(key: String, value: T) {
        when (value) {
            is Boolean -> {
                sharedPreferences.edit().putBoolean(key, (value as Boolean)).apply()
            }
            is String -> {
                sharedPreferences.edit().putString(key, value as String).apply()
            }
            is Long -> {
                sharedPreferences.edit().putLong(key, (value as Long)).apply()
            }
            is Int -> {
                sharedPreferences.edit().putInt(key, (value as Int)).apply()
            }
            else -> {
                sharedPreferences.edit().putString(key, GsonUtils.toJson(value)).apply()
            }
        }
    }

    override fun getString(key: String): String {
        return sharedPreferences.getString(key, AppConstants.DEFAULT_STRING_VALUE)!!
    }

    override fun getBoolean(key: String): Boolean {
        return sharedPreferences.getBoolean(key, false)
    }

    override fun getBoolean(key: String, defaultValue: Boolean): Boolean {
        return sharedPreferences.getBoolean(key, defaultValue)
    }

    override fun getInt(key: String): Int {
        return sharedPreferences.getInt(key, AppConstants.DEFAULT_ERROR_VALUE)
    }

    override fun getInt(key: String, defaultValue: Int): Int {
        return sharedPreferences.getInt(key, defaultValue)
    }

    override fun getLong(key: String): Long {
        return sharedPreferences.getLong(key, AppConstants.DEFAULT_ERROR_VALUE.toLong())
    }

    override fun clear() {
        sharedPreferences.edit().clear().apply()
    }

    override fun deleteKey(key: String) {
        if (sharedPreferences.contains(key)) {
            sharedPreferences.edit().remove(key).apply()
        }
    }

    override fun contain(key: String): Boolean {
        return sharedPreferences.contains(key)
    }
}
package com.project.testapplication.base

import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.blankj.utilcode.util.LogUtils
import java.util.concurrent.atomic.AtomicBoolean
//this class from google examples
class SingleLiveEvent<T> : MutableLiveData<T>()
{
    private val mPending = AtomicBoolean(false)

    @MainThread
    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        if (hasActiveObservers()) {
            removeObservers(owner)
            super.observe(owner) { t: T? ->
                if (mPending.compareAndSet(true, false)) {
                    observer.onChanged(t)
                }
            }
        }

        // Observe the internal MutableLiveData
        super.observe(owner) { t: T? ->
            if (mPending.compareAndSet(true, false)) {
                observer.onChanged(t)
            }
        }
    }

    @MainThread
    override fun setValue(t: T?) {
        mPending.set(true)
        super.setValue(t)
    }

    /**
     * Used for cases where T is Void, to make calls cleaner.
     */
    @MainThread
    fun call() {
        setValue(null)
    }
}
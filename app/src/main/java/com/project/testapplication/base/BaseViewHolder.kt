package com.project.testapplication.base

import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder(private val viewDataBinding: ViewDataBinding) : RecyclerView.ViewHolder(viewDataBinding.root) {

    init {
        viewDataBinding.root.setOnClickListener { view: View? -> onViewHolderClicked(adapterPosition) }
    }

    fun executePendingBindings() {
        viewDataBinding.executePendingBindings()
    }

    abstract fun bind(position: Int)
    abstract fun onViewHolderClicked(position: Int)
}
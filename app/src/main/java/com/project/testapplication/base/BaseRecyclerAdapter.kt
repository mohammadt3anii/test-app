package com.project.testapplication.base

import android.content.Context
import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerAdapter<OBJECT>(var context: Context) :
    RecyclerView.Adapter<BaseViewHolder>() {

    private val items = ArrayList<OBJECT>()
    var inflater: LayoutInflater = LayoutInflater.from(context)

    constructor(context: Context, newList: List<OBJECT>?) : this(context) {
        items.clear()
        newList?.let {
            items.addAll(newList)
        }

    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind(position)
        holder.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return getListSize()
    }

    fun getListSize(): Int {
        return items.size
    }

    fun setList(newList: List<OBJECT>?) {
        items.clear()
        newList?.let {
            items.addAll(it)
            notifyDataSetChanged()
        }
    }

    fun getList(): List<OBJECT> {
        return items
    }

    fun getItem(position: Int): OBJECT {
        return items[position]
    }

    fun addAll(items: List<OBJECT>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun addItem(item: OBJECT) {
        val index = items.size
        items.add(item)
        notifyItemInserted(index)
    }

    fun addItem(index: Int, item: OBJECT) {
        items.add(index, item)
        notifyItemInserted(index)
    }

    fun addItemBeforeLastIndex(item: OBJECT) {
        val index = items.size - 1
        items.add(index, item)
        notifyItemInserted(index)
    }

    fun any(predicate: (OBJECT) -> Boolean): Boolean {
        return items.any(predicate)
    }

    fun find(predicate: (OBJECT) -> Boolean): OBJECT? {
        return items.find(predicate)
    }
}
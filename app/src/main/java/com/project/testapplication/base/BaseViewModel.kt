package com.project.testapplication.base

import android.app.Application
import androidx.lifecycle.*
import com.blankj.utilcode.util.GsonUtils
import com.blankj.utilcode.util.LogUtils
import com.project.testapplication.data.local.SharedPreferenceManager
import com.project.testapplication.data.remote.ApiService
import com.project.testapplication.models.ErrorMessage
import com.project.testapplication.models.NetworkResult
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Response
import java.lang.Exception
import javax.inject.Inject

abstract class BaseViewModel : ViewModel(), LifecycleObserver {
    private val disposable: CompositeDisposable = CompositeDisposable()

    @Inject
    lateinit var sharedPreferenceManager: SharedPreferenceManager

    @Inject
    lateinit var application: Application

    fun <R, T> makeRequest(
        singleLiveEvent: SingleLiveEvent<NetworkResult<R?>>? = null,
        request: suspend () -> Response<T>,
        onSuccess: ((NetworkResult.Success<T?>) -> Unit)? = null,
        onError: ((NetworkResult.Error<T?>) -> Unit)? = null
    ) {
        singleLiveEvent?.postValue(NetworkResult.Loading())
        viewModelScope.launch {
            try {
                val response: Response<T> = request()
                if (response.isSuccessful) {
                    onSuccess?.invoke(NetworkResult.Success(response.body()))
                } else {
                    if (response.errorBody() != null) {
                        try {
                            val error: String = response.errorBody()!!.string()
                            val mainErrorHandling =
                                GsonUtils.fromJson(error, ErrorMessage::class.java)
                            onError?.invoke(NetworkResult.Error(mainErrorHandling = mainErrorHandling))
                        } catch (e: Exception) {

                            onError?.invoke(NetworkResult.Error(throwable = e))
                        }
                    } else {
                        onError?.invoke(NetworkResult.Error())
                    }
                }
//                }
            } catch (ce: CancellationException) {
                throw ce
            } catch (e: Exception) {
                onError?.invoke(NetworkResult.Error(throwable = e))
            }
        }
    }

    fun <T> subscribeOnMainThread(single: Single<T>, callback: SingleObserver<T>) {
        single.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(object : SingleObserver<T> {
                override fun onSubscribe(d: Disposable) {
                    callback.onSubscribe(d)
                    addDisposable(d)
                }

                override fun onSuccess(t: T) {
                    callback.onSuccess(t)
                }

                override fun onError(e: Throwable) {
                    callback.onError(e)
                }
            })
    }

    private fun addDisposable(d: Disposable?) {
        disposable.add(d!!)
    }

    fun removeDisposable(d: Disposable?) {
        if (!d!!.isDisposed) {
            disposable.dispose()
        }
        disposable.delete(d)
    }

    override fun onCleared() {
        super.onCleared()
        clearDisposable()
    }

    private fun clearDisposable() {
        if (disposable.size() > 0) {
            if (!disposable.isDisposed) {
                disposable.dispose()
                disposable.clear()
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    abstract fun onCreate(owner: LifecycleOwner?)

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    abstract fun onStart(owner: LifecycleOwner?)

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    abstract fun onResume(owner: LifecycleOwner?)

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    abstract fun onPause(owner: LifecycleOwner?)

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    abstract fun onStop(owner: LifecycleOwner?)

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    abstract fun onDestroy(owner: LifecycleOwner?)
}
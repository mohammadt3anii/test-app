package com.project.testapplication.viewModels

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.SavedStateHandle
import com.project.testapplication.base.BaseViewModel
import com.project.testapplication.base.SingleLiveEvent
import com.project.testapplication.data.remote.ApiService
import com.project.testapplication.models.NetworkResult
import com.project.testapplication.models.images.ImageItemResponseItem
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AppViewModel @Inject constructor(savedStateHandle: SavedStateHandle/*this is to save any value between fragments if needed*/) :
    BaseViewModel() {

    @Inject
    lateinit var apiService: ApiService

    val imagesEvent = SingleLiveEvent<NetworkResult<List<ImageItemResponseItem>?>>()
    fun getListOfImages() {
        makeRequest(
            singleLiveEvent = imagesEvent,
            request = { apiService.listOfImages() },
            onSuccess = {
                imagesEvent.postValue(it)
            },
            onError = {

            })
    }

    override fun onCreate(owner: LifecycleOwner?) {

    }

    override fun onStart(owner: LifecycleOwner?) {

    }

    override fun onResume(owner: LifecycleOwner?) {

    }

    override fun onPause(owner: LifecycleOwner?) {

    }

    override fun onStop(owner: LifecycleOwner?) {

    }

    override fun onDestroy(owner: LifecycleOwner?) {

    }
}
package com.project.testapplication

import android.os.Bundle
import android.widget.LinearLayout
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.project.testapplication.binding.ImagesAdapter
import com.project.testapplication.databinding.ActivityMainBinding
import com.project.testapplication.ext.isInternetConnected
import com.project.testapplication.models.NetworkResult
import com.project.testapplication.models.images.ImageItemResponseItem
import com.project.testapplication.viewModels.AppViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val viewModel by viewModels<AppViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        setupViews()
        requestImages()
        observers()
    }
  
    private fun setupViews() {
        binding.imagesRecycler.layoutManager = LinearLayoutManager(this)
        binding.imagesRecycler.setHasFixedSize(true)
    }

    private fun requestImages() {
        isInternetConnected {
            viewModel.getListOfImages()
        }
    }

    private fun observers() {
        viewModel.imagesEvent.observe(this, {
            when (it) {
                is NetworkResult.Error -> {
                    // show error if need to show
                }
                is NetworkResult.Loading -> {
                    // show loading if needed
                }
                is NetworkResult.Success -> {
                    presentAdapter(it.data)
                }
            }
        })
    }

    lateinit var adapter: ImagesAdapter
    private fun presentAdapter(list: List<ImageItemResponseItem>?) {
        if (!::adapter.isInitialized) {
            adapter = ImagesAdapter(this, list!!) {
                ImageItemActivity.startActivity(this,it)
            }
            binding.imagesRecycler.adapter = adapter
        } else {
            adapter.setList(list)
        }
    }

}
package com.project.testapplication.models.images

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ImageItemResponseItem(
    @SerializedName("alt_description")
    val altDescription: String? = null,
    @SerializedName("blur_hash")
    val blurHash: String? = null,

    @SerializedName("color")
    val color: String? = null,
    @SerializedName("created_at")
    val createdAt: String? = null,

    @SerializedName("description")
    val description: String? = null,
    @SerializedName("height")
    val height: Int? = null,
    @SerializedName("id")
    val id: String? = null,
    @SerializedName("liked_by_user")
    val likedByUser: Boolean? = null,
    @SerializedName("likes")
    val likes: Int? = null,
    @SerializedName("links")
    val links: ImageLinks? = null,
    @SerializedName("promoted_at")
    val promotedAt: String? = null,
    @SerializedName("sponsorship")
    val sponsorship: Sponsorship? = null,
    @SerializedName("updated_at")
    val updatedAt: String? = null,
    @SerializedName("urls")
    val urls: Urls? = null,
    @SerializedName("user")
    val user: User? = null,
    @SerializedName("width")
    val width: Int? = null
) : Parcelable

@Parcelize
data class ImageLinks(
    @SerializedName("download")
    val download: String? = null,
    @SerializedName("download_location")
    val downloadLocation: String? = null,
    @SerializedName("html")
    val html: String? = null,
    @SerializedName("self")
    val self: String? = null
) : Parcelable

@Parcelize
data class Sponsorship(

    @SerializedName("sponsor")
    val sponsor: Sponsor? = null,
    @SerializedName("tagline")
    val tagline: String? = null,
    @SerializedName("tagline_url")
    val taglineUrl: String? = null
) : Parcelable

@Parcelize
data class UserLinks(
    @SerializedName("followers")
    val followers: String? = null,
    @SerializedName("following")
    val following: String? = null,
    @SerializedName("html")
    val html: String? = null,
    @SerializedName("likes")
    val likes: String? = null,
    @SerializedName("photos")
    val photos: String? = null,
    @SerializedName("portfolio")
    val portfolio: String? = null,
    @SerializedName("self")
    val self: String? = null
) : Parcelable

@Parcelize
data class ProfileImage(
    @SerializedName("large")
    val large: String? = null,
    @SerializedName("medium")
    val medium: String? = null,
    @SerializedName("small")
    val small: String? = null
) : Parcelable

@Parcelize
data class Social(
    @SerializedName("portfolio_url")
    val portfolioUrl: String? = null,
    @SerializedName("twitter_username")
    val twitterUsername: String? = null
) : Parcelable

@Parcelize
data class Urls(
    @SerializedName("full")
    val full: String? = null,
    @SerializedName("raw")
    val raw: String? = null,
    @SerializedName("regular")
    val regular: String? = null,
    @SerializedName("small")
    val small: String? = null,
    @SerializedName("thumb")
    val thumb: String? = null
) : Parcelable

@Parcelize
data class User(
    @SerializedName("accepted_tos")
    val acceptedTos: Boolean? = null,
    @SerializedName("bio")
    val bio: String? = null,
    @SerializedName("first_name")
    val firstName: String? = null,
    @SerializedName("for_hire")
    val forHire: Boolean? = null,
    @SerializedName("id")
    val id: String? = null,
    @SerializedName("instagram_username")
    val instagramUsername: String? = null,
    @SerializedName("last_name")
    val lastName: String? = null,
    @SerializedName("links")
    val links: UserLinks? = null,
    @SerializedName("location")
    val location: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("portfolio_url")
    val portfolioUrl: String? = null,
    @SerializedName("profile_image")
    val profileImage: ProfileImage? = null,
    @SerializedName("social")
    val social: Social? = null,
    @SerializedName("total_collections")
    val totalCollections: Int? = null,
    @SerializedName("total_likes")
    val totalLikes: Int? = null,
    @SerializedName("total_photos")
    val totalPhotos: Int? = null,
    @SerializedName("twitter_username")
    val twitterUsername: String? = null,
    @SerializedName("updated_at")
    val updatedAt: String? = null,
    @SerializedName("username")
    val username: String? = null
) : Parcelable

@Parcelize
data class Sponsor(
    @SerializedName("accepted_tos")
    val acceptedTos: Boolean? = null,
    @SerializedName("bio")
    val bio: String? = null,
    @SerializedName("first_name")
    val firstName: String? = null,
    @SerializedName("for_hire")
    val forHire: Boolean? = null,
    @SerializedName("id")
    val id: String? = null,

    @SerializedName("last_name")
    val lastName: String? = null,
    @SerializedName("links")
    val links: UserLinks? = null,

    @SerializedName("name")
    val name: String? = null,
    @SerializedName("portfolio_url")
    val portfolioUrl: String? = null,
    @SerializedName("profile_image")
    val profileImage: ProfileImage? = null,
    @SerializedName("social")
    val social: Social? = null,
    @SerializedName("total_collections")
    val totalCollections: Int? = null,
    @SerializedName("total_likes")
    val totalLikes: Int? = null,
    @SerializedName("total_photos")
    val totalPhotos: Int? = null,
    @SerializedName("twitter_username")
    val twitterUsername: String? = null,
    @SerializedName("updated_at")
    val updatedAt: String? = null,
    @SerializedName("username")
    val username: String? = null
) : Parcelable
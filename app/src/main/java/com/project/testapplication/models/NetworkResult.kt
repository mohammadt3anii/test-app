package com.project.testapplication.models

import com.google.gson.annotations.SerializedName

sealed class NetworkResult<T>(
    var data: T? = null,
    var error: ErrorMessage? = null,
    var throwable: Throwable? = null
) {

    class Success<T>(data: T? = null) : NetworkResult<T>(data = data)
    class Error<T>(
        data: T? = null,
        mainErrorHandling: ErrorMessage? = null,
        throwable: Throwable? = null
    ) : NetworkResult<T>(data = data, error = mainErrorHandling, throwable = throwable)

    class Loading<T> : NetworkResult<T>()

}

data class ErrorMessage(@SerializedName("message") var error: String? = null)
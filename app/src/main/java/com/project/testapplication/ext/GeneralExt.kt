package com.project.testapplication.ext

import com.blankj.utilcode.util.NetworkUtils
import com.blankj.utilcode.util.ToastUtils


fun isInternetConnected(function: () -> Unit) {
    if (NetworkUtils.isConnected()) {
        function()
    } else {
        ToastUtils.showLong("Please check the internet connection")
    }
}
package com.project.testapplication.di

import com.google.gson.GsonBuilder
import com.project.testapplication.BuildConfig
import com.project.testapplication.constants.AppConstants
import com.project.testapplication.data.remote.ApiService
import com.project.testapplication.data.remote.AuthInterceptor
import com.project.testapplication.data.local.SharedPreferenceManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {


    @Provides
    fun provideAuthInterceptor( sharedPreferenceManager: SharedPreferenceManager): AuthInterceptor {
        return AuthInterceptor( sharedPreferenceManager)
    }

    @Provides
    fun provideOkHttpClient(interceptor: HttpLoggingInterceptor, authInterceptor: AuthInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addNetworkInterceptor(interceptor)
            .addInterceptor(authInterceptor)
            .readTimeout(AppConstants.TIME_OUT, AppConstants.TIME_UNIT)
            .connectTimeout(AppConstants.TIME_OUT, AppConstants.TIME_UNIT)
            .build()
    }
    @Provides
    fun provideAdServerRetrofit(okHttpClient: OkHttpClient, /*io: Scheduler,*/ factory: GsonConverterFactory): Retrofit {
        return Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
            //.addCallAdapterFactory(RxJava2CallAdapterFactoryCustom.createWithScheduler(io))
            .addConverterFactory(factory)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }
    @Provides
    fun provideFactory(): GsonConverterFactory {
        return GsonConverterFactory.create(
            GsonBuilder()
//                .setDateFormat(AppConstants.DEFAULT_DATE_FORMAT)
                .create())
    }

    @Provides
    fun provideRetrofitService( retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }
    @Provides
    fun provideInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            interceptor.level = HttpLoggingInterceptor.Level.BODY
        } else {
            interceptor.level = HttpLoggingInterceptor.Level.NONE
        }
        return interceptor
    }
}
package com.project.testapplication.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.project.testapplication.data.local.SharedPreferenceDataSource
import com.project.testapplication.data.local.SharedPreferenceManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object InternalStorageModule {
    private const val PREF_NAME = "pref_file"
    @Provides
    fun getSharedPreferences(context: Application): SharedPreferences {
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }
    @Provides
    fun getSharedPreferenceManager(sharedPreferences: SharedPreferenceDataSource,preferences: SharedPreferences): SharedPreferenceManager {
        return SharedPreferenceManager(sharedPreferences,preferences)
    }
    @Provides
    fun sharedPreferenceDataSource(preferences: SharedPreferences): SharedPreferenceDataSource {
        return SharedPreferenceDataSource(preferences)
    }
}
package com.project.testapplication.binding

import android.content.Context
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.project.testapplication.R
import com.project.testapplication.base.BaseRecyclerAdapter
import com.project.testapplication.base.BaseViewHolder
import com.project.testapplication.databinding.AdapterImageRowBinding
import com.project.testapplication.models.images.ImageItemResponseItem


class ImagesAdapter(
    context: Context,
    list: List<ImageItemResponseItem>,
    val onItemSelected: (ImageItemResponseItem) -> Unit
) : BaseRecyclerAdapter<ImageItemResponseItem>(context, list) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: AdapterImageRowBinding =
            DataBindingUtil.inflate(inflater, R.layout.adapter_image_row, parent, false)
        binding.lifecycleOwner = context as AppCompatActivity
        return ItemViewHolder(binding)
    }

    private inner class ItemViewHolder(private val binding: AdapterImageRowBinding) :
        BaseViewHolder(binding) {
        override fun bind(position: Int) {
            binding.item = getItem(position)
            Glide.with(context).load(getItem(position).urls?.small).into(binding.imageItem)
        }

        override fun onViewHolderClicked(position: Int) {
            onItemSelected(getItem(position))
        }
    }

}
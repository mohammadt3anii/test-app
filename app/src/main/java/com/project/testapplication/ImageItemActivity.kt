package com.project.testapplication

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.blankj.utilcode.util.LogUtils
import com.bumptech.glide.Glide
import com.project.testapplication.databinding.ActivityImageItemBinding
import com.project.testapplication.models.images.ImageItemResponseItem
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ImageItemActivity : AppCompatActivity() {
    private lateinit var binding: ActivityImageItemBinding

    companion object {
        private const val DATA_KEY = "data"
        fun startActivity(activity: AppCompatActivity, data: ImageItemResponseItem) {
            val intent = Intent(activity,ImageItemActivity::class.java).apply {
                putExtra(DATA_KEY, data)
            }

            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_image_item)
        binding.lifecycleOwner = this
        intent.extras.let { bundle ->
            bundle?.getParcelable<ImageItemResponseItem>(DATA_KEY).let {
                Glide.with(binding.photoView).load(it?.urls?.full).into(binding.photoView)
            }

        }

    }
}